package app.service;

import org.springframework.core.io.UrlResource;
import org.springframework.web.multipart.MultipartFile;

public interface StorageService {

    void store(MultipartFile file);

    UrlResource loadFile(String filename);

    void deleteAll();

    void init();

}
