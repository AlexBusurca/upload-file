package com.startit.uploadfile.controller;

import app.service.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class UploadController {

    @Autowired
    StorageService fileStorageService;

    List<String> files = new ArrayList<String>();

    @GetMapping("/")
    public String listUploadedFile(Model model) {
        return "WEB-INF/uploadForm";
    }

    @PostMapping("/")
    public String handleFileUpload(@RequestParam("file") MultipartFile file, Model model) {
        try {
            fileStorageService.store(file);
            model.addAttribute("message", "File " + file.getOriginalFilename() + " was uploaded successfully!");
            files.add(file.getOriginalFilename());
        } catch (Exception e) {
            model.addAttribute("message", "Failed to upload" + file.getOriginalFilename());
        }
        return "WEB-INF/uploadForm";
    }

    @GetMapping("/listuplaodedfiles")
    public String getUploadedFiles(Model model) {
        model.addAttribute("files",
                files.stream()
                        .map(fileName -> MvcUriComponentsBuilder
                                .fromMethodName(UploadController.class, "getFile", fileName).build().toString())
                        .collect(Collectors.toList()));
        model.addAttribute("totalFiles", "TotalFiles: " + files.size());
        return "WEB-INF/listFiles";
    }

    @GetMapping("/files/{fileName:.+}")
    @ResponseBody
    public ResponseEntity<Resource> getFile(@PathVariable String fileName) {
        UrlResource file = fileStorageService.loadFile(fileName);
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() +"\"")
                .body(file);
    }

}
