package com.startit.uploadfile;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import javax.annotation.Resource;


@SpringBootApplication
public class fileUploaderApp {

    @Resource
    app.service.StorageService fileStorageService;

    public static void main(String[] args) {
        SpringApplication.run(fileUploaderApp.class, args);
    }

    @Bean
    CommandLineRunner init(app.service.StorageService storageService) {
        return (args) -> {
            fileStorageService.deleteAll();
            fileStorageService.init();
        };
    }
}
